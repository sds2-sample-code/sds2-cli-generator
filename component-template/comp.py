from Designable.ProcessableComponent import ProcessableComponent
from sds2.utility.gadget_protocol import GadgetComponent
from $version_module_name import $version_class_name
from $edit_module_name import BuildComponentUI
from Point3D import Point3D
from kwargmixin.kwargmixin import KWArgMixin


@$version_class_name
class $object_name(GadgetComponent, ProcessableComponent, KWArgMixin):
    """
    Include a brief description of the component
    """

    def __init__(self, **kwargs):
        $version_class_name.init_factory()(self, **kwargs) #  run the init from the
                                             #  versioning class
        ProcessableComponent.__init__(self, **kwargs)

        KWArgMixin.__init__(self, **kwargs)

    def IsAllowedOnMember(self, mn):
        """Determine whether or not the component is allowed on the member
        
        This method will be called in modeling whenever a component add
        is started. It will control whether or not a member is allowed to be
        chosen; it will also determine which components are shown in the 
        component add list if members are preselected.

        Args:
            mn (int): Member number of the member being considered
        
        Returns:
            bool: True if the component can be added to the member
                  False otherwise
        """

        return True


    def SetReferencePointForMemberUI(self, mn):
        """Set the reference point for the component on the member

        The reference point is relative to the left end of the host member
        
        Args:
            mn (int): Member number of the member hosting the component
        """

        # because the point is relative, (0., 0., 0.) will return the 
        # left end of the host member as the reference point for the component
        self.ref_point = Point3D(0., 0., 0.)

        # this can be left out if the left end of the member is acceptable
        # as the reference point

    def Add(self, mn):
        """Take care of any user input needed to add the component
        
        Args:
            mn (int): Member number of the member hosting the component
        
        Returns:
            bool: True allows the component to continue; False will stop the
                  process of adding the component
        """
        # take care of any necessary user input here, e.g. point location
        return True

    @classmethod
    def Factory(cls, host, member_universe):
        """Instantiate, initialize, and return an instance of the component
        
        Args:
            cls ($object_name): the component class
            host (int): Member number for the component host
            member_universe (list): List of members related to the component
        
        Returns:
            Component: An instantiated version of the component class
        """
        comp = cls()  # create an instance of the component
        # Set any attributes
        comp.x = 4
        return comp

    # @staticmethod is required for this method
    # this is part of the Gadget Protocol interface
    @staticmethod
    def CreateCustomMultiEditableUI(model, gadget_factory):
        """Create the UI for the component
        
        Args:
            model (list): List of component objects considered for edit
            gadget_factory (GadgetFactory): Convenience class for creating
                                            items in the screen
        """
        BuildComponentUI(model, gadget_factory)

    def DesignForMember(self, mn):
        """Bulk of the work for adding the component is done here

        Any material should be added in this method
        
        Args:
            mn (int): Member number of the member hosting the component
        
        Returns:
            bool: True allows the component to continue; False will stop the
                  process of adding the component
        """

        return True