import os
import sys

if os.path.dirname(__file__) not in sys.path:
    sys.path.append(os.path.dirname(__file__))

from $object_name import ${object_name}Runner

${object_name}Runner().Run()