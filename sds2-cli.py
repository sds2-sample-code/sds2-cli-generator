import argparse
import os

from string import Template


COMPONENT_TEMPLATE_DIR = 'component-template'
COMPONENT_BASE_TEMPLATE_DIR = 'comp-component-base'
MEMBER_TEMPLATE_DIR = 'custom-member-template'
TOOL_TEMPLATE_DIR = 'tool-template'


template_dir_path = os.path.dirname(os.path.realpath(__file__))


parser = argparse.ArgumentParser(
    description="""A CLI for creating SDS/2 custom objects directories and
                   files"""
)


parser.add_argument(
    'object_type',
    type=str,
    help='Type of custom object to create',
    choices=['component', 'component-base', 'member', 'tool']
)


parser.add_argument(
    'name',
    type=str,
    help='Name for the new custom object'

)


parser.add_argument(
    '-d', '--directory',
    help='Directory where the custom object will be created'
)


if __name__ == '__main__':
    arg_space = parser.parse_args()

    substitution_files = {
        'component': {
            'object-file': os.path.join(
                COMPONENT_TEMPLATE_DIR,
                'comp.py'
            ),
            'Screen.py': os.path.join(
                COMPONENT_TEMPLATE_DIR,
                'comp-screen.py'
            ),
            'Versions.py': os.path.join(
                COMPONENT_TEMPLATE_DIR,
                'comp-versions.py'
            ),
            'PLUGIN': os.path.join(COMPONENT_TEMPLATE_DIR, 'PLUGIN'),
            '__init__.py': os.path.join(COMPONENT_TEMPLATE_DIR, '__init__.py')
        },
        'component-base': {
            'object-file': os.path.join(
                COMPONENT_BASE_TEMPLATE_DIR,
                'comp.py'
            ),
            'Screen.py': os.path.join(
                COMPONENT_BASE_TEMPLATE_DIR,
                'comp-screen.py'
            ),
            'Versions.py': os.path.join(
                COMPONENT_BASE_TEMPLATE_DIR,
                'comp-versions.py'
            ),
            'PLUGIN': os.path.join(COMPONENT_BASE_TEMPLATE_DIR, 'PLUGIN'),
            '__init__.py': os.path.join(COMPONENT_BASE_TEMPLATE_DIR, '__init__.py')
        },
        'member': {
            'object-file': os.path.join(
                MEMBER_TEMPLATE_DIR,
                'member.py'
            ),
            'Screen.py': os.path.join(
                MEMBER_TEMPLATE_DIR,
                'member-screen.py'
            ),
            'Versions.py': os.path.join(
                MEMBER_TEMPLATE_DIR,
                'member-versions.py'
            ),
            'PLUGIN': os.path.join(MEMBER_TEMPLATE_DIR, 'PLUGIN'),
            '__init__.py': os.path.join(MEMBER_TEMPLATE_DIR, '__init__.py')
        },
        'tool': {
            'object-file': os.path.join(
                TOOL_TEMPLATE_DIR,
                'tool.py'
            ),
            '%s_command.py' % (arg_space.name): os.path.join(
                TOOL_TEMPLATE_DIR,
                'tool_command.py'
            ),
            '%s_runner.py' % (arg_space.name): os.path.join(
                TOOL_TEMPLATE_DIR,
                'tool_runner.py'
            ),
            'PLUGIN': os.path.join(TOOL_TEMPLATE_DIR, 'PLUGIN')
        }
    }

    main_file_name = "%s.py" % (arg_space.name)

    replacement = {
        'object_name': arg_space.name,
        'edit_module_name': "Screen",
        'version_module_name': "Versions",
        'version_class_name': "%sVersions" % (arg_space.name)
    }

    working_directory = (
        arg_space.directory
        if arg_space.directory
        else os.getcwd()
    )

    destination = os.path.join(working_directory, arg_space.name)

    try:
        os.mkdir(destination)
    except OSError as ose:
        print(ose.message)

    for file_name, tmplt_file in substitution_files[
            arg_space.object_type.lower()
        ].items():
        if file_name == 'object-file':
            working_file_name = main_file_name
        else:
            working_file_name = file_name

        with open(os.path.join(template_dir_path, tmplt_file)) as tmplt:
            src = Template(tmplt.read())
            result = src.substitute(replacement)

            with open(
                os.path.join(
                    destination,
                    working_file_name
                ),
                'w+'
            ) as outfile:
                outfile.write(result)
