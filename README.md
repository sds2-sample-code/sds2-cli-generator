A command line interface for generating SDS/2 custom object boiler plate.

## Installation
### Linux
Add an alias to your terminal profile (i.e. .bashrc)
```bash
alias sds2-cli='python /path/to/sds2-cli/sds2-cli.py'
```

### Windows
Windows is a bit tricker, [here](https://superuser.com/questions/560519/how-to-set-an-alias-in-windows-command-line) is a good place to start figuring out the process of adding the command.

## Use
```
usage: sds2-cli.py [-h] [-d DIRECTORY] {component,member,tool} name

A CLI for creating SDS/2 custom objects directories and files

positional arguments:
  {component,member,tool}
                        Type of custom object to create
  name                  Name for the new custom object

optional arguments:
  -h, --help            show this help message and exit
  -d DIRECTORY, --directory DIRECTORY
                        Directory where the custom object will be created
```

e.g.
```
sds2-cli component ShinyNewComponent -d /path/to/desired/directory/
```

if the -d flag is omitted, the script will create the files in the current directory.